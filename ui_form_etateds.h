/********************************************************************************
** Form generated from reading UI file 'form_etateds.ui'
**
** Created: Tue 26. Jul 16:52:56 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_ETATEDS_H
#define UI_FORM_ETATEDS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form_etatEds
{
public:
    QTableWidget *tableActivite;
    QLabel *label;
    QRadioButton *radioButton_actif;
    QRadioButton *radioButton_inactif;
    QLineEdit *lineEdit_rchFiness;
    QLabel *label_rchFiness;
    QPushButton *btn_raz;

    void setupUi(QWidget *Form_etatEds)
    {
        if (Form_etatEds->objectName().isEmpty())
            Form_etatEds->setObjectName(QString::fromUtf8("Form_etatEds"));
        Form_etatEds->resize(496, 553);
        tableActivite = new QTableWidget(Form_etatEds);
        tableActivite->setObjectName(QString::fromUtf8("tableActivite"));
        tableActivite->setGeometry(QRect(10, 100, 471, 431));
        label = new QLabel(Form_etatEds);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(220, 10, 61, 21));
        radioButton_actif = new QRadioButton(Form_etatEds);
        radioButton_actif->setObjectName(QString::fromUtf8("radioButton_actif"));
        radioButton_actif->setGeometry(QRect(20, 70, 51, 17));
        radioButton_inactif = new QRadioButton(Form_etatEds);
        radioButton_inactif->setObjectName(QString::fromUtf8("radioButton_inactif"));
        radioButton_inactif->setGeometry(QRect(80, 70, 61, 17));
        lineEdit_rchFiness = new QLineEdit(Form_etatEds);
        lineEdit_rchFiness->setObjectName(QString::fromUtf8("lineEdit_rchFiness"));
        lineEdit_rchFiness->setGeometry(QRect(370, 70, 113, 20));
        label_rchFiness = new QLabel(Form_etatEds);
        label_rchFiness->setObjectName(QString::fromUtf8("label_rchFiness"));
        label_rchFiness->setGeometry(QRect(300, 70, 61, 16));
        btn_raz = new QPushButton(Form_etatEds);
        btn_raz->setObjectName(QString::fromUtf8("btn_raz"));
        btn_raz->setGeometry(QRect(150, 70, 75, 21));

        retranslateUi(Form_etatEds);

        QMetaObject::connectSlotsByName(Form_etatEds);
    } // setupUi

    void retranslateUi(QWidget *Form_etatEds)
    {
        Form_etatEds->setWindowTitle(QApplication::translate("Form_etatEds", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form_etatEds", "Activit\303\251 EDS", 0, QApplication::UnicodeUTF8));
        radioButton_actif->setText(QApplication::translate("Form_etatEds", "Actif", 0, QApplication::UnicodeUTF8));
        radioButton_inactif->setText(QApplication::translate("Form_etatEds", "Inactif", 0, QApplication::UnicodeUTF8));
        label_rchFiness->setText(QApplication::translate("Form_etatEds", "N\302\260 FINESS :", 0, QApplication::UnicodeUTF8));
        btn_raz->setText(QApplication::translate("Form_etatEds", "R.A.Z", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form_etatEds: public Ui_Form_etatEds {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_ETATEDS_H
