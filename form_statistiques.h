#ifndef FORM_STATISTIQUES_H
#define FORM_STATISTIQUES_H

#include <QWidget>
#include <QMessageBox>
#include <QTableWidget>

#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include <QDesktopServices>

#include <QUrl>

#include <QCompleter>

#include "database.h"

namespace Ui {
class Form_statistiques;
}

class Form_statistiques : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_statistiques(QWidget *parent = 0);
    ~Form_statistiques();

    void afficherIHM(QTableWidget *table, QStringList liste);
    
private slots:
    void on_calendrier_clicked(const QDate &date);

    void on_btn_genererStats_clicked();

    void on_btn_expoterStats_clicked();

    void on_lineEdit_finess_textChanged(const QString &arg1);

private:
    Ui::Form_statistiques *ui;

    Database bdd;
    QStringList header;
};

#endif // FORM_STATISTIQUES_H
