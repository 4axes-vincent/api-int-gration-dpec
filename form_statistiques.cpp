#include "form_statistiques.h"
#include "ui_form_statistiques.h"

#define NOM_FICHIER_EXPORT           QString(QCoreApplication::applicationDirPath () % "/Rapport/Export_" % QDateTime::currentDateTime ().toString ("dd_MM_yyyy_hh_mm_ss") % ".csv" )

Form_statistiques::Form_statistiques(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_statistiques)
{
    ui->setupUi(this);
    this->setWindowTitle ( ui->label_titre->text () );

    this->setFixedSize (666,663);

    ui->comboBox_finess->insertItem(0, "Tous");
    QStringList listeFiness = bdd.listerColonne ("tbl_apiIntegration", "finess");
    listeFiness.removeDuplicates ();
    ui->comboBox_finess->addItems ( listeFiness );

    QCompleter *completer = new QCompleter( listeFiness );
    ui->lineEdit_finess->setCompleter ( completer );


    header << "Finess" << "Date traitement" << "Heure traitement" << "Nombre DPEC" << "Nombre erreur" << "Nombre rejet�";
    ui->tableStats->setColumnCount ( header.length () );
    ui->tableStats->setHorizontalHeaderLabels ( header );
    ui->tableStats->horizontalHeader ()->setResizeMode (QHeaderView::Stretch);

    ui->lineEdit_date->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_finess->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_nbDpec->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_nbErreur->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_nbRejete->setAlignment ( Qt::AlignCenter );

    ui->lineEdit_date->setText ( QDate::currentDate ().toString ("dd/MM/yyyy") );
}




//----------------------------------------------------------------------------------------------------------------------
Form_statistiques::~Form_statistiques()
{
    delete ui;
}










//----------------------------------------------------------------------------------------------------------------------
void Form_statistiques::afficherIHM(QTableWidget *table, QStringList liste){

    if( !liste.isEmpty () ){

        table->setRowCount (0);

        int nbDpec = 0;
        int nbErreur = 0;
        int nbRejete = 0;

        for(int i = 0 ; i < liste.length () ; i++){

            table->insertRow (i);

            QStringList listeDecoupee = liste.at (i).split (";");

            for(int j = 0 ; j < listeDecoupee.length () ; j++){

                QTableWidgetItem *item = new QTableWidgetItem( listeDecoupee.at (j) );
                item->setTextAlignment (Qt::AlignCenter);

                if( j == header.indexOf ("Nombre DPEC") )
                    nbDpec += listeDecoupee.at (j).toInt ();

                else if( j == header.indexOf ("Nombre erreur") )
                    nbErreur += listeDecoupee.at (j).toInt ();

                else if( j == header.indexOf ("Nombre rejet�") )
                    nbRejete += listeDecoupee.at (j).toInt ();

                table->setItem (i,j, item);
            }
        }

        ui->lineEdit_nbDpec->setText ( QString::number ( nbDpec ) );
        ui->lineEdit_nbErreur->setText ( QString::number ( nbErreur ) );
        ui->lineEdit_nbRejete->setText ( QString::number ( nbRejete ) );
    }
    else
        QMessageBox::information (NULL,"R�sultat recherche", "Aucune statistique");
}













//----------------------------------------------------------------------------------------------------------------------
void Form_statistiques::on_calendrier_clicked(const QDate &date){

    ui->lineEdit_date->setText ( date.toString ("dd/MM/yyyy") );
}













//----------------------------------------------------------------------------------------------------------------------
void Form_statistiques::on_btn_genererStats_clicked(){

    if( !ui->lineEdit_date->text ().isEmpty () ){

        if( ui->comboBox_finess->currentText () == "Tous" )
            afficherIHM( ui->tableStats,
                         bdd.getStatsComplete ("tbl_apiIntegration", "-1", ui->lineEdit_date->text () ) );
        else
            afficherIHM( ui->tableStats,
                         bdd.getStatsComplete ("tbl_apiIntegration", ui->comboBox_finess->currentText (),
                                               ui->lineEdit_date->text () ) );

    }
    else
        QMessageBox::warning (NULL,"S�lection date", "Veuillez s�lectionner une date");

}

















//----------------------------------------------------------------------------------------------------------------------
void Form_statistiques::on_btn_expoterStats_clicked(){

    if( ui->tableStats->rowCount () > 0 ){

        QFile fichierStats( NOM_FICHIER_EXPORT );
        if( !fichierStats.open ( QIODevice::Text | QIODevice::ReadWrite | QIODevice::Append ) )
            QMessageBox::warning (NULL,"Ouverture fichier", "Impossible d'ouvrir le fichier " % NOM_FICHIER_EXPORT
                                  % " - Erreur : " % fichierStats.errorString () );
        else{

            QTextStream fluxStats(&fichierStats);

            for(int i = 0 ; i < ui->tableStats->rowCount () ; i++){

                fluxStats << ui->tableStats->item (i,0)->text ()
                                % ";" % ui->tableStats->item (i,1)->text ()
                                % ";" % ui->tableStats->item (i,2)->text ()
                                % ";" % ui->tableStats->item (i,3)->text ()
                                % ";" % ui->tableStats->item (i,4)->text ()
                                % ";" % ui->tableStats->item (i,5)->text () << endl;

            }

            fluxStats << "Nombre de DPEC : " % ui->lineEdit_nbDpec->text () << endl;
            fluxStats << "Nombre d'erreur : " % ui->lineEdit_nbErreur->text () << endl;
            fluxStats << "Nombre de rejet� : " % ui->lineEdit_nbRejete->text () << endl;

            fichierStats.close ();

            int resultat = QMessageBox::question (NULL,"Ouverture dossier", "Souhaitez-vous ouvrir le dossier destination ?",
                                                  QMessageBox::Yes | QMessageBox::No);

            if( resultat == QMessageBox::Yes ){

                QFileInfo fichierInfo( NOM_FICHIER_EXPORT );

                QDesktopServices::openUrl(QUrl("file:///"+ fichierInfo.absolutePath (),QUrl::TolerantMode) );
            }
        }
    }
    else
        QMessageBox::warning (NULL,"Export r�sultat", "Aucune donn�es exportable");

}


















//----------------------------------------------------------------------------------------------------------------------
void Form_statistiques::on_lineEdit_finess_textChanged(const QString &arg1){

    for(int i = 0 ; i < ui->tableStats->rowCount () ; i++){

        if( !ui->tableStats->item (i,0)->text ().contains ( arg1 ) )
            ui->tableStats->setRowHidden (i, true);

        else
            ui->tableStats->setRowHidden (i, false);
    }

}
