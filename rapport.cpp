#include "rapport.h"

#define NOM_FICHIER_STATS           QString(QCoreApplication::applicationDirPath () % "/Rapport/statistique_" % QDate::currentDate ().toString ("dd_MM_yyyy") % ".csv" )

Rapport::Rapport()
{
}








//----------------------------------------------------------------------------------------------------------------------
void Rapport::genererRapport(){

    Database *bdd = new Database();
    connect (bdd, SIGNAL(on_message(QString)), this, SLOT(receptionMessage(QString)));

    QStringList liste = bdd->getStats ();

    QThread::currentThread ()->msleep (1000);

    liste.push_front ("Finess;Nombre DPEC;Nombre erreur;Nombre rejet�");
    liste.push_front ("Date : " % QDate::currentDate ().toString ("dd/MM/yyyy"));

    QFile fichierStats( NOM_FICHIER_STATS );
    if( !fichierStats.open ( QIODevice::Text | QIODevice::ReadWrite) )
        emit on_erreur("Impossible d'ouvrir le fichier " % NOM_FICHIER_STATS % " - Erreur : " % fichierStats.errorString () );
    else{

        QTextStream fluxStats(&fichierStats);

        fluxStats << liste.join ("\n") << endl;

        fluxStats << "Nombre de DPEC : " % bdd->getStats ("nbDpec") << endl;
        fluxStats << "Nombre d'erreur : " % bdd->getStats ("nbErreur") << endl;
        fluxStats << "Nombre de rejet� : " % bdd->getStats ("nbRejete") << endl;

        fichierStats.close ();
    }

    delete bdd;

    QThread::currentThread ()->msleep (1000);

    emit on_traitementFini( NOM_FICHIER_STATS );
}










//----------------------------------------------------------------------------------------------------------------------
void Rapport::receptionMessage(QString message){

    emit on_erreur (message);
}
