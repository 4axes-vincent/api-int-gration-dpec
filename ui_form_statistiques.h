/********************************************************************************
** Form generated from reading UI file 'form_statistiques.ui'
**
** Created: Thu 21. Jul 15:56:36 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_STATISTIQUES_H
#define UI_FORM_STATISTIQUES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form_statistiques
{
public:
    QTableWidget *tableStats;
    QLabel *label_titre;
    QComboBox *comboBox_finess;
    QLabel *label;
    QCalendarWidget *calendrier;
    QLineEdit *lineEdit_date;
    QPushButton *btn_genererStats;
    QPushButton *btn_expoterStats;
    QLineEdit *lineEdit_finess;
    QLineEdit *lineEdit_nbDpec;
    QLineEdit *lineEdit_nbErreur;
    QLineEdit *lineEdit_nbRejete;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QFrame *line;

    void setupUi(QWidget *Form_statistiques)
    {
        if (Form_statistiques->objectName().isEmpty())
            Form_statistiques->setObjectName(QString::fromUtf8("Form_statistiques"));
        Form_statistiques->resize(666, 633);
        tableStats = new QTableWidget(Form_statistiques);
        tableStats->setObjectName(QString::fromUtf8("tableStats"));
        tableStats->setGeometry(QRect(10, 210, 641, 401));
        label_titre = new QLabel(Form_statistiques);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(290, 20, 61, 16));
        comboBox_finess = new QComboBox(Form_statistiques);
        comboBox_finess->setObjectName(QString::fromUtf8("comboBox_finess"));
        comboBox_finess->setGeometry(QRect(310, 140, 121, 22));
        label = new QLabel(Form_statistiques);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(310, 110, 61, 21));
        calendrier = new QCalendarWidget(Form_statistiques);
        calendrier->setObjectName(QString::fromUtf8("calendrier"));
        calendrier->setGeometry(QRect(10, 40, 256, 155));
        lineEdit_date = new QLineEdit(Form_statistiques);
        lineEdit_date->setObjectName(QString::fromUtf8("lineEdit_date"));
        lineEdit_date->setGeometry(QRect(310, 80, 113, 20));
        btn_genererStats = new QPushButton(Form_statistiques);
        btn_genererStats->setObjectName(QString::fromUtf8("btn_genererStats"));
        btn_genererStats->setGeometry(QRect(480, 170, 81, 23));
        btn_expoterStats = new QPushButton(Form_statistiques);
        btn_expoterStats->setObjectName(QString::fromUtf8("btn_expoterStats"));
        btn_expoterStats->setGeometry(QRect(570, 170, 81, 23));
        lineEdit_finess = new QLineEdit(Form_statistiques);
        lineEdit_finess->setObjectName(QString::fromUtf8("lineEdit_finess"));
        lineEdit_finess->setGeometry(QRect(310, 170, 113, 20));
        lineEdit_nbDpec = new QLineEdit(Form_statistiques);
        lineEdit_nbDpec->setObjectName(QString::fromUtf8("lineEdit_nbDpec"));
        lineEdit_nbDpec->setGeometry(QRect(540, 70, 113, 20));
        lineEdit_nbErreur = new QLineEdit(Form_statistiques);
        lineEdit_nbErreur->setObjectName(QString::fromUtf8("lineEdit_nbErreur"));
        lineEdit_nbErreur->setGeometry(QRect(540, 100, 113, 20));
        lineEdit_nbRejete = new QLineEdit(Form_statistiques);
        lineEdit_nbRejete->setObjectName(QString::fromUtf8("lineEdit_nbRejete"));
        lineEdit_nbRejete->setGeometry(QRect(540, 130, 113, 20));
        label_2 = new QLabel(Form_statistiques);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(490, 70, 41, 21));
        label_3 = new QLabel(Form_statistiques);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(490, 100, 41, 21));
        label_4 = new QLabel(Form_statistiques);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(490, 130, 41, 21));
        line = new QFrame(Form_statistiques);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(470, 70, 20, 91));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        retranslateUi(Form_statistiques);

        QMetaObject::connectSlotsByName(Form_statistiques);
    } // setupUi

    void retranslateUi(QWidget *Form_statistiques)
    {
        Form_statistiques->setWindowTitle(QApplication::translate("Form_statistiques", "Form", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("Form_statistiques", "Statistiques", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form_statistiques", "Finess C.H :", 0, QApplication::UnicodeUTF8));
        btn_genererStats->setText(QApplication::translate("Form_statistiques", "G\303\251n\303\251rer", 0, QApplication::UnicodeUTF8));
        btn_expoterStats->setText(QApplication::translate("Form_statistiques", "Exporter", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Form_statistiques", "DPEC :", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Form_statistiques", "Erreur :", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Form_statistiques", "Rejet\303\251", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form_statistiques: public Ui_Form_statistiques {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_STATISTIQUES_H
