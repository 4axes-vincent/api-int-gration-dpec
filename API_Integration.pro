#-------------------------------------------------
#
# Project created by QtCreator 2015-09-07T14:25:03
#
#-------------------------------------------------

QT       += core gui concurrent sql

QT += xml

QT += network

TARGET = API_Integration
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    decoupage.cpp \
    configuration.cpp \
    database.cpp \
    form_statistiques.cpp \
    rapport.cpp \
    form_etateds.cpp

HEADERS  += mainwindow.h \
    decoupage.h \
    configuration.h \
    database.h \
    form_statistiques.h \
    rapport.h \
    form_etateds.h

FORMS    += mainwindow.ui \
    form_statistiques.ui \
    form_etateds.ui

RC_FILE = ressource.rc
