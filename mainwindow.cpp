#include "mainwindow.h"
#include "ui_mainwindow.h"


/*
  API int�gration DPEC
  */




//----------------------------------------------------------------------------------------------------------------------

#define NOM_FICHIER_ENVOI               QString("fichierDPEC.csv")
#define NOM_FICHIER_LOG_CIBLE           QString("log_cible.txt")
#define NOM_FICHIER_RAPPORT             QString(QCoreApplication::applicationDirPath () % "/rapport.log")

#define PATH_FICHIER_CONFIGURATION      QString(QCoreApplication::applicationDirPath () % "/config.ini")
#define NOM_DOSSIER_BACKUP_LOG          QString("backupLog")
#define NOM_DOSSIER_ERREUR_DPEC         QString("ERREUR_ENVOI_DPEC")

#define HTTP_DOMAINE_4AXES              QString("domaine")
#define HTTP_DOMAINE_4AXES_VALEUR       QString("4axes")


//Serveur local
//#define ADR_HTTP_INTEGRATION            QString("http://192.168.1.12/transmissions/httpsdocs/api/envoicsv")

//Serveur pr�-prod
//#define ADR_HTTP_INTEGRATION            QString("https://pp.4axes.net/api/envoicsv")

//Serveur prod
#define ADR_HTTP_INTEGRATION            QString("https://4axes.net/api/envoicsv")



//----------------------------------------------------------------------------------------------------------------------
/*
  Constructeur
  */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle ("API 4@xes - Int�gration fichier");
    this->setFixedSize (684,574);

    QStringList listeArguments = QCoreApplication::arguments ();

    ui->statusBar->showMessage ( ADR_HTTP_INTEGRATION );

    //[nomProgramme.exe nomFichierCSV]
    if( listeArguments.length () >= 4 ){

        fermerIHM = false;

        if( listeArguments.at (3) == "-FERMER_FIN:ON")
            fermerIHM = true;

        //R�cup�re le nom du fichier a d�couper et envoyer
        nomFichier = listeArguments.at (1);
        finess = listeArguments.at (2);

        //Cela veut dire qu'on pr�cise un �diteur -> important pour r�cup�rer les informations "NOMBENEF"
        if( listeArguments.length () == 5 )
            editeur = listeArguments.at (4);

        fichierLogCible = NOM_FICHIER_LOG_CIBLE % "_" % finess % ".txt";

        fichierCSVInfo = nomFichier;

        NOM_FICHIER_LOG = "Integration_" % finess % "_" % dateTime.currentDateTime ().toString ("yyyy-MM-dd_hh_mm_ss") % ".log";

        ui->lineEdit_idCH->setText ( finess );

        chronoEnreg = new QTime();
        chronoTotal = new QTime();

        //Variable de stats
        nbEnregistrement = 0;
        nbErreur = 0;
        nbRejete = 0;

        //Lancement
        lancerIntegration( nomFichier );
    }
    else if( listeArguments.length () == 2 ){

        if( listeArguments.at (1) == "rapport" ){

            Rapport *rapport = new Rapport();
            connect (rapport, SIGNAL(on_erreur(QString)), this, SLOT(afficherErreur(QString)));
            connect (rapport, SIGNAL(on_traitementFini(QString)), this, SLOT(finRapport(QString)));
            connect (rapport, SIGNAL(on_message(QString)), this, SLOT(afficherMessage(QString)));

            QtConcurrent::run(rapport, &Rapport::genererRapport);
        }
    }
}


//----------------------------------------------------------------------------------------------------------------------
/*
  Destructeur
  */
MainWindow::~MainWindow(){

    delete ui;
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Premi�re �tape de l'int�gration du fichier
  */
void MainWindow::lancerIntegration (QString nomFichierCSV){

    //Chrono pour calculer le temps total d'ex�cution
    chronoTotal->start ();

    //IHM
    ui->lineEdit_adrSite->setText ( ADR_HTTP_INTEGRATION );
    ui->lineEdit_adrSite->setReadOnly ( true );
    ui->lineEdit_adrSite->home (true);

    ui->lineEdit_nomFichier->setText ( nomFichier );
    ui->lineEdit_nomFichier->setReadOnly ( true );

    ecrireIHM ("Lancement int�gration fichier " % nomFichier);
    ecrireIHM ("--------------------------------------");

    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,"********************************************");
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,"********************************************");
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Lancement API  sur " % ADR_HTTP_INTEGRATION);
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "--------------------------------------");
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Int�gration du fichier " % nomFichierCSV);


    decoupage *decouper = new decoupage();

    connect (decouper, SIGNAL(on_ligneDecouper(QPair<QString,QString>, QString)), this,
             SLOT(envoyerFichier(QPair<QString,QString>, QString)), Qt::DirectConnection);

    connect ( decouper, SIGNAL(on_traitementFini()), this, SLOT(integrationFini()) );
    connect ( decouper, SIGNAL(on_nombreLigneChange(int)), this, SLOT(afficherResultat(int)) );
    connect ( decouper, SIGNAL(on_erreurDestinataire(QString)), this, SLOT(destinataireVide(QString)) );
    connect ( decouper, SIGNAL(on_erreur(QString)), this, SLOT(afficherErreur(QString)) );

    connect ( this, SIGNAL( on_pause(bool) ), decouper, SLOT(setPause(bool) ) );

    QtConcurrent::run(decouper, &decoupage::decouperFichier, nomFichierCSV);
}

































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de debugger
  */
void MainWindow::debugger (QString titre, QString info){

    QMessageBox::information (NULL, titre, info);
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi un fichier au format CSV compos� d'une ligne d'ent�te + une ligne de donn�e
  */
void MainWindow::envoyerFichier ( QPair<QString, QString> pairDonnee, QString idDestinataire ){

    if( !pairDonnee.first.isEmpty () && !finess.isEmpty () ){

        emit on_pause ( true );

        chronoEnreg->start ();

        //Cr�ation fichier temporaire pour envoi sur la plateforme
        QFile fichierCSV( NOM_FICHIER_ENVOI );
        if( ! fichierCSV.open ( QIODevice::Text | QIODevice::WriteOnly ) )
            debugger ("Ouverture fichier", "Impossible d'ouvrir le fichier " % NOM_FICHIER_ENVOI % " | Erreur " % fichierCSV.errorString () );
        else{

            QTextStream fluxCSV(&fichierCSV);

            fluxCSV << pairDonnee.first << endl;
            fluxCSV << pairDonnee.second << endl;

            fichierCSV.close ();
        }

        if( !QFile::exists ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_ENVOI ) ){

            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                        "Le fichier a envoy� n'existe pas ou a �t� d�plac� - Int�gration annul�e");

            ecrireLog (QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,"-----------------------------------");

            ecrireIHM ("Le fichier " % NOM_FICHIER_ENVOI % " n'existe pas ou a �t� d�plac� - Int�gration annul�e");
        }
        else{

            ecrireLog (QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                       "Envoi du fichier sur " % ADR_HTTP_INTEGRATION);

            QProcess processus;

            //Lancer CURL avec idMesage / pathFichier / url 4axes.net
            QString cmd = QCoreApplication::applicationDirPath ()
                    % "/CURL/curl.exe -k -H " % HTTP_DOMAINE_4AXES % ":" % HTTP_DOMAINE_4AXES_VALEUR
                    % " --form origine=\"\" --form idClientEmetteur=" % finess % " --form idClientDestinataire=" % idDestinataire
                    % " --form \"fichier=@" % fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_ENVOI
                    % "\" " % ADR_HTTP_INTEGRATION;

            //Lancement de la commande avec cUrl.exe
            processus.start ( cmd );

            //On lit la sortie pour connaitre le resultat de notre requ�te
            processus.setProcessChannelMode(QProcess::MergedChannels);
            processus.waitForFinished(-1);
            QString resultatPrompt = processus.readAll ();

            if( resultatPrompt.isEmpty () ){

                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "Erreur processus : " % processus.errorString () );

                ecrireIHM ("Erreur processus : " % processus.errorString ());
            }
            else
                //Analyse de la r�ponse
                parserReponse (resultatPrompt, pairDonnee.second );



        }
    }
    else{

        ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                    "V�rifier la paire de donn�es (ent�te - donn�es) / le FINESS ou le RNM");

        ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                    "*************************  Fin transaction  *************************" );
        close();
    }
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'alimenter un fichier de trace/log dont le nom et les informations sont pass�s en param�tre
  */
void MainWindow::ecrireLog (QString fichier, QString info){

    QFile fichierLog(fichier);
    if( ! fichierLog.open ( QIODevice::Text | QIODevice::WriteOnly | QIODevice::Append ) )
        debugger ("Ouverture fichier log","Impossible d'ouvrir le fichier " % fichier % " - " % fichierLog.errorString ());

    else{

        QDateTime dateTime;

        QTextStream fluxLog(&fichierLog);

        fluxLog << dateTime.currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss")
                    << " >> "
                    << info
                    << endl;

        fichierLog.close ();
    }
}

















//----------------------------------------------------------------------------------------------------------------------
/*
   Permet d'�crire sur l'IHM
   */
void MainWindow::ecrireIHM(QString message){

    ui->textEdit->append ( QDateTime::currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss") % " : " % message );
}





























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de parser la r�ponse en XML retourn�e par le serveur
  */
void MainWindow::parserReponse (QString reponse, QString ligneDonnee){

    if( !reponse.isEmpty () ){

        if( reponse.contains ( "Connexion refusee" ) )
            debugger ("Erreur", "Connexion refus�e : IP non autoris�e / Plateforme en maintenance / Nom de domaine erron�");
        else{

            int temps = chronoEnreg->elapsed ();

            ecrireLog ( QCoreApplication::applicationDirPath () % "/" %  NOM_FICHIER_LOG,
                        "Temps d'envoi : " % QString::number ( temps )% " ms" );

            ecrireIHM ("Temps d'envoi : " % QString::number ( temps ) % " ms");

            QXmlStreamReader xmlReader(reponse);

            bool reconcilier = false;

            QString erreur;
            QString detail;
            QString statutDPEC;
            QStringList listeDetail;
            QString idChronoDPEC;

            QStringList pairDecoupee = ligneDonnee.split (";");

            QString nomBenef;
            QString numEntree = pairDecoupee.at (3);
            QString rnm = pairDecoupee.at (4);

            //On va chercher le nomBenef � un autre endroit pour les fichiers provenant de CPAGE
            if( editeur == "cpage" )
                nomBenef = pairDecoupee.at (9);

            else
                nomBenef = pairDecoupee.at (16);

            int nbDetail = 1;

            while( !xmlReader.atEnd () && !xmlReader.hasError ()){

                xmlReader.readNext ();

                //Si c'est une valise de d�but et la balise est <Reponse>
                if( xmlReader.isStartElement () && xmlReader.name ().toString () == "Reponse" ){

                    //Si l'attribut "resultat" est � 1 (OK)
                    if (xmlReader.attributes ().value ("resultat") == "1")
                        reconcilier = true;
                }

                if( reconcilier ){

                    if( xmlReader.name ().toString () == "idChrono" )
                         idChronoDPEC = xmlReader.readElementText ();

                    if( xmlReader.name ().toString () == "statutDPEC" )
                         statutDPEC = xmlReader.readElementText ();
                }
                else if( !reconcilier ){

                    while( xmlReader.isStartElement () ){

                        if( xmlReader.name ().toString () == "erreur" )
                            erreur = xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "statutDPEC" )
                                statutDPEC = xmlReader.readElementText ();

                        else if( xmlReader.name ().toString ().contains ( "infos" ) ){

                            detail = xmlReader.readElementText ();
                            listeDetail.push_back ( "D�tail " % QString::number (nbDetail) % " : " % detail );
                            nbDetail++;
                        }
                        xmlReader.readNext ();
                    }
                }
            }

            //Si la r�conciliation a �t� effectu�e
            if ( reconcilier ){

                ecrireIHM ("DPEC int�gr�e - " % idChronoDPEC);
                ecrireIHM ("Statut DPEC : " % statutDPEC);
                ecrireIHM ("-------------------");

                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "IDCHRONO " % idChronoDPEC % " a �t� int�gr� � la plateforme 4AXES.NET");

                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "Statut DPEC : " % statutDPEC);
                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "-----------------------------------------------------------");

            }
            else{

                QString libelle;

                if( statutDPEC == "rejete" ){

                    nbRejete++;
                    statutDPEC = "Rejet�";
                    libelle = "Impossible d'envoyer la DPEC - Statut : " % statutDPEC % " - [ Num. entr�e : "
                            % numEntree % " | Nom b�n�ficiaire : " % nomBenef % " | code O.C : " % rnm % " ] -- Erreur : "
                            % erreur;
                }
                else{

                    nbErreur++;
                    libelle =  "Impossible d'int�grer la DPEC - Statut : Non int�gr� - [ Num. entr�e : " % numEntree
                            % " | Nom b�n�ficiaire : " % nomBenef % " | code O.C : " % rnm % " ] -- Erreur : " % erreur;
                }


                ecrireIHM ("Impossible d'int�grer la DPEC pour l'idChrono " % idChronoDPEC);
                ecrireIHM ("Erreur : " % erreur);

                //S'il y a des d�tails
                if( ! listeDetail.isEmpty () ){

                    //On parcours la liste
                    for( int j = 0 ; j < listeDetail.length () ; j++ ){

                        ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, listeDetail.at (j) );
                        ecrireIHM (listeDetail.at(j));
                    }
                    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                                "-----------------------------------------------------------");
                }
                else{

                    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, libelle );

                    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, libelle  );

                }

                ecrireIHM ("----------------------");

                //D�placer le fichier en cas d'erreur pour le sauvegarder
                deplacerFichier( NOM_FICHIER_ENVOI, NOM_DOSSIER_ERREUR_DPEC );

                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "-----------------------------------------------------------");
            }

             ui->textEdit->viewport ()->update ();
             emit on_pause ( false );
        }
    }
    else{

        ecrireIHM ("R�ponse du serveur vide");
        ui->zoneErreur->append ("R�ponse du serveur vide");
        nbErreur++;
    }
}













//----------------------------------------------------------------------------------------------------------------------
void MainWindow::destinataireVide(QString ligneDonnee){

    QString numEntree = ligneDonnee.split (";").at (3);
    QString nomBenef = ligneDonnee.split (";").at (16);
    QString msg = "Impossible d'int�grer la DPEC [ Num. entr�e : " % numEntree
            % " | Nom b�n�f. : " % nomBenef % " | code O.C : Non renseign� ] ";

    ecrireIHM ( msg );
    ecrireIHM ("----------------------");

    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, msg );

    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                "-----------------------------------------------------------");
    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, msg );

    nbErreur++;
}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::testReponse (){


//    QString data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
//     data.append ("<Reponse resultat=\"1\"> \n" );
//     data.append ( "<info>\n" );
//     data.append ( "<statutDPEC>Remis</statutDPEC>\n" );
//     data.append ( "<idChrono>111111111_222222222_20151102150266</idChrono>\n" );
//     data.append ( "</info>\n" );
//     data.append ( "</Reponse>\n" );

     QString data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
      data.append ("<Reponse resultat=\"0\"> \n" );
      data.append ( "<erreur>Impossible d'integrer la DPEC</erreur>\n" );
      data.append ( "<detail>Insertion �chou�e</detail>\n" );
      data.append ( "</Reponse>\n" );


//      parserReponse ( data);
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de d�placer un fichier "nomFichier" dans un r�pertoire "repDestination"
  */
void MainWindow::deplacerFichier (QString pathFichier, QString repDestination){

    //V�rifier que toutes les informations sont pr�sentes
    if( !pathFichier.isEmpty () && !repDestination.isEmpty () ){

        //Info du fichier a traiter
        QFileInfo fichierInfo(pathFichier);

        QDir dossier;
        QString nomFichierACopier = "echecDPEC_" % dateTime.currentDateTime ().toString ("yyyyMMdd_hh_mm_ss")% ".csv";

        bool flagDossier = true;

        //Si le dossier existe
        if( !dossier.exists ( fichierInfo.absolutePath () % "/" % repDestination ) ){

            //Cr�ation du dossier
            if ( !dossier.mkdir (repDestination ) ){

                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "Impossible de cr�er le dossier " % repDestination % " impossible");
                debugger ("Cr�ation dossier","Impossible de cr�er le dossier " % repDestination);

                flagDossier = false;
            }
        }

        //Si le dossier de destination existe bien
        if( flagDossier ){

            QFile fichier;

            //Si un fichier dans le r�pertoire existe d�j� renomme le fichier arrivant
            if( fichier.exists ( fichierInfo.absolutePath () % "/" % repDestination % "/" % nomFichierACopier ) )
                nomFichierACopier.append ( "_new_" % dateTime.currentDateTime ().toString ("yyyyMMdd_hh_mm_ss") );

            //Si la copie est impossible
            if( !fichier.copy ( fichierInfo.filePath (), fichierInfo.absolutePath () % "/" % repDestination % "/" % nomFichierACopier ) ){

                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "Impossible de copier le fichier "
                            % nomFichierACopier % " -> " % fichier.errorString () );

                ui->zoneErreur->setPlainText ("Impossible de d�placer le fichier trait� dans le dossier "
                                             % repDestination % " | Erreur -> " % fichier.errorString () );
            }
            else{

                //Si la suppression apr�s copie est impossible
                if( !fichier.remove ( fichierInfo.filePath () ) ){

                    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                                "Impossible de supprimer le fichier "
                               % pathFichier % " -> " % fichier.errorString () );

                    ui->zoneErreur->setPlainText ("Impossible de supprimer le fichier "
                                                  % pathFichier % " -> " % fichier.errorString () );
                }
                else
                    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                                "Fichier d�plac� dans " % repDestination );
            }
        }
        else{

            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                        "Impossible de d�placer le fichier " % pathFichier
                        % " - Le dossier " % repDestination % " n'a pas pu �tre cr��" );

            ui->zoneErreur->setPlainText ("Impossible de d�placer le fichier " % pathFichier
                                          % " - Le dossier " % repDestination % " n'a pas pu �tre cr��" );
        }
    }
    else{

        //Si le nom fichier vide
        if( pathFichier.isEmpty () ){

            ui->zoneErreur->append ("Nom du fichier manquant");
            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                        "Nom du fichier � d�placer dans " % repDestination % " manquant ");
        }
        else if( repDestination.isEmpty () ){ // Si nom r�pertoire vide

            ui->zoneErreur->append ("Nom du r�pertoire des erreurs d'envoi manquant");
            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                        "Nom du r�pertoire des fichiers en erreur manquant" );
        }
    }
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le bouton "Ouvrir log"
  */
void MainWindow::on_btn_ouvrirLog_clicked(){

    QFile fichier( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG );
    if( fichier.exists () )
        QDesktopServices::openUrl(QUrl("file:///"+ QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                                       QUrl::TolerantMode) );
    else
        debugger ("Ouverture fichier","Impossible d'ouvrir le fichier " % NOM_FICHIER_LOG % " | Erreur : " % fichier.errorString () );
}































//----------------------------------------------------------------------------------------------------------------------
void MainWindow::importerTrace(){

    QFile fichierIntegration( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG);
    if( ! fichierIntegration.open ( QIODevice::Text | QIODevice::ReadOnly ) )
        debugger ("Ouverture fichier", "Impossible d'ouvrir le fichier " % NOM_FICHIER_LOG
                  % " | Erreur : " % fichierIntegration.errorString () );
    else{

        QTextStream fluxIntegration(&fichierIntegration);

        ecrireIHM ( fluxIntegration.readAll () );
    }
}











//----------------------------------------------------------------------------------------------------------------------
void MainWindow::envoyerMail(){

    Configuration *config = new Configuration( PATH_FICHIER_CONFIGURATION );

    //On test si le groupe [finess] existe dans le fichier de configuration
    if( config->siGroupeExiste ( finess ) ){

        QStringList listeCleMail = config->listeCle ( finess );

        for(int i = 0 ; i < listeCleMail.length () ; i++){

            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Envoi e-mail � " % config->getValue ( finess, listeCleMail.at (i) ) );

            system( config->getValue ( "Path","Postie" ).toLocal8Bit ()
                    + " -host:" + (config->getValue ( "Config_mail", "host" ) ).toLocal8Bit ()
                    + " -to:" + (config->getValue ( finess, listeCleMail.at (i) ) ).toLocal8Bit ()
                    + " -from:" + (config->getValue ( "Config_mail", "from" ) ).toLocal8Bit ()
                    + " -s:\"" + (config->getValue ( "Config_mail","sujet" ) + " " + finess).toLocal8Bit () + "\""
                    + " -nomsg -a:" + ( fichierCSVInfo.absolutePath () + "/" + NOM_FICHIER_LOG_CIBLE ).toLocal8Bit () ) ;

            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "E-mail envoy�" );

            Sleep(100);
        }
    }
    else{

        ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                    "E-MAIL NON ENVOYE - La configuration e-mail du "
                    % finess % " n'a pas �t� trouv� dans le fichier de config." );

        ui->textEdit->append ( "E-MAIL NON ENVOYE -  La configuration e-mail du " % finess
                               % " n'a pas �t� trouv� dans le fichier de config." );
    }
}









































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de backuper le fichier "log_cible" dans le r�pertoire NOM_DOSSIER_BACKUP_LOG
  */
void MainWindow::backuperlog(){

    bool dossierExiste = true;
    QDir dossier;

    //Si le dossier de destination existe
    if( !dossier.exists ( fichierCSVInfo.absolutePath () % "/" % NOM_DOSSIER_BACKUP_LOG ) ){

        if( !dossier.mkpath ( fichierCSVInfo.absolutePath () % "/" % NOM_DOSSIER_BACKUP_LOG ) ){

            ecrireIHM ( "Impossible de cr�er le r�pertoire " % NOM_DOSSIER_BACKUP_LOG );
            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                        "Impossible de cr�er le r�pertoire " % NOM_DOSSIER_BACKUP_LOG );

            dossierExiste = false;
        }
    }
    if( dossierExiste ){

        QFile fichierLog( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE );

        if( fichierLog.copy ( fichierCSVInfo.absolutePath () % "/" % NOM_DOSSIER_BACKUP_LOG % "/" % NOM_FICHIER_LOG_CIBLE
                              % "_" % finess % "_" % QDateTime::currentDateTime ().toString ("yyyyMMdd_hh_mm_ss") % ".txt" ) ){

            if( fichierLog.remove () ){

                ecrireIHM ("Fichier log_cible deplac� avec succ�s");
                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Fichier log_cible deplac� avec succ�s");
            }
            else{

                ecrireIHM ( "Impossible de supprimer le fichier log_cible - Erreur : " % fichierLog.errorString () );
                ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                            "Impossible de supprimer le fichier log_cible - Erreur : " % fichierLog.errorString () );
            }
        }
        else{

            ecrireIHM ( "Impossible de d�placer le fichier log_cible - Erreur : " % fichierLog.errorString () );
            ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                        "Impossible de d�placer le fichier log_cible - Erreur : " % fichierLog.errorString () );
        }
    }
}














//----------------------------------------------------------------------------------------------------------------------
void MainWindow::afficherErreur(QString message){

    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, message);
    ecrireIHM ( message );
    debugger ("Message", message);
}

















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::afficherMessage(QString message){

    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, message );
    ecrireIHM ( message );
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Affichage du temps total d'int�gration du fichier
  */
void MainWindow::integrationFini(){

    int tempsTotal = chronoTotal->elapsed ();
    ui->label_tempsRequete->setText ("Temps total : " % QString::number ( tempsTotal ) % " ms");

    ui->textEdit->append ( "Total enregistrement(s) : " % QString::number ( nbEnregistrement ) );
    ui->textEdit->append ( "Total erreur(s) : " %  QString::number ( nbErreur ) );
    ui->textEdit->append ( "Total rejet�(s) : " %  QString::number ( nbRejete ) );
    ui->textEdit->append ( "Total DPEC envoy�e(s) : " %  QString::number ( nbEnregistrement - nbErreur - nbRejete ) );
    ui->textEdit->append ( "*************************  Fin transaction  *************************" );

    /*
      0 => finess
      1 => date courante
      2 => heure:minute:seconde
      3 => nombre de DPEC trait�es
      4 => nombre de DPEC en erreur
      5 => nombre de DPEC rejet�e
      */
    QStringList listeDonnees;
    listeDonnees.push_back ( finess );
    listeDonnees.push_back ( QDateTime::currentDateTime ().toString ("dd/MM/yyyy") );
    listeDonnees.push_back ( QDateTime::currentDateTime ().toString ("hh:mm:ss") );
    listeDonnees.push_back ( QString::number ( nbEnregistrement - nbErreur - nbRejete ) );
    listeDonnees.push_back ( QString::number ( nbErreur ) );
    listeDonnees.push_back ( QString::number ( nbRejete ) );

    Database *bdd = new Database();
    connect ( bdd, SIGNAL(on_message(QString)), this, SLOT(afficherMessage(QString)) );

    if( bdd->ajouterDonnee ("tbl_apiIntegration", listeDonnees) ){

        ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Statistiques ajout�es en base de donn�es" );
        ecrireIHM ( "Statistiques ajout�es en base de donn�es" );
    }
    else{

        ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Impossible d'ajouter les statistiques en base de donn�es" );
        ecrireIHM ( "Impossible d'ajouter les statistiques en base de donn�es" );
    }

    delete bdd;

    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Temps total de traitement : " % QString::number ( tempsTotal )  % " ms");
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Total enregistrement(s) : " % QString::number ( nbEnregistrement ) );
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Total erreur(s) : " %  QString::number ( nbErreur ) );
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Total rejet�(s) : " %  QString::number ( nbRejete ) );
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "Total DPEC int�gr�e(s) : " %  QString::number ( nbEnregistrement - nbErreur ) );
    ecrireLog ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG, "*************************  Fin transaction  *************************" );

    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, "***************" );
    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, "Total enregistrement(s) : " % QString::number ( nbEnregistrement ) );
    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, "Total erreur(s) : " %  QString::number ( nbErreur ) );
    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, "Total rejet�(s) : " %  QString::number ( nbRejete ) );
    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, "Total DPEC int�gr�e(s) : " %  QString::number ( nbEnregistrement - nbErreur ) );
    ecrireLog ( fichierCSVInfo.absolutePath () % "/" % NOM_FICHIER_LOG_CIBLE, "*************************  Fin transaction  *************************" );

    envoyerMail ();

    backuperlog ();

    ui->textEdit->setReadOnly ( true );

    if( fermerIHM )
        QCoreApplication::quit ();

}

















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::afficherResultat(int nbLigne ){

    nbEnregistrement = nbLigne;

    ui->lineEdit_nbEnregistrement->setText ( QString::number ( nbEnregistrement ) );
    ui->lineEdit_nbErreur->setText ( QString::number ( nbErreur ) );
    ui->lineEdit_nbRPEC->setText ( QString::number ( nbEnregistrement - nbErreur ) );
}













//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionStatistiques_triggered(){

    Form_statistiques *stats = new Form_statistiques();
    stats->show ();
}










//----------------------------------------------------------------------------------------------------------------------
void MainWindow::finRapport(QString fichierRapport){

    Configuration *config = new Configuration( PATH_FICHIER_CONFIGURATION );

    system( config->getValue ( "Path","Postie" ).toLocal8Bit ()
            + " -host:" + (config->getValue ( "Config_mail", "host" ) ).toLocal8Bit ()
            + " -to:" + (config->getValue ( "Config_mail", "to_rapport" ) ).toLocal8Bit ()
            + " -from:" + (config->getValue ( "Config_mail", "from" ) ).toLocal8Bit ()
            + " -s:\"" + (config->getValue ( "Config_mail","sujet_rapport" ) + " " + finess).toLocal8Bit () + "\""
            + " -nomsg -a:" + ( fichierRapport  ).toLocal8Bit () ) ;

    delete config;

    QCoreApplication::quit ();
}













//----------------------------------------------------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent *event){

    int resultat = QMessageBox::question (NULL,"Confirmer fermeture", "Souhaitez-vous quitter le logiciel ?",
                                          QMessageBox::Yes | QMessageBox::No);

    if( resultat == QMessageBox::Yes )
        QCoreApplication::quit ();

    else
        event->ignore ();
}














//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionSauvegarde_logs_triggered(){

    QStringList filtre;
    filtre << "*.log";

    QFileInfo fichierInfo;
    QFile fichier;

    QDir dossier;

    bool siDossierCree = true;

    QDirIterator iterateurLog(QCoreApplication::applicationDirPath (), filtre, QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks );

    while( iterateurLog.hasNext () ){

        fichierInfo = iterateurLog.next ();

        QStringList nomFichierDecoupee = fichierInfo.baseName ().split ("_");

        dossier.setPath ( QCoreApplication::applicationDirPath () % "/LOGS/" % nomFichierDecoupee.at (1) );

        if( !dossier.exists () ){

            if( !dossier.mkpath ( dossier.absolutePath () ) ){

                afficherErreur ("Erreur lors de la cr�ation du dossier : " % nomFichierDecoupee.at (1) );
                siDossierCree = false;
            }
        }

        if( siDossierCree ){

            if( fichier.copy ( fichierInfo.absoluteFilePath (), dossier.absolutePath () % "/" % fichierInfo.fileName () ) ){

                if( !fichier.remove ( fichierInfo.absoluteFilePath () ) )
                    afficherErreur ("Impossible de supprimer le fichier [ " % fichierInfo.absoluteFilePath () % " ] - " % fichier.errorString () );
            }
            else
                afficherErreur ("Impossible de copier le fichier [ " % fichierInfo.absoluteFilePath () % " ] - " % fichier.errorString () );
        }
    }

    QMessageBox::information (NULL, "Sauvegarde log", "Sauvegarde log termin�e");
}





//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionActivit_EDS_triggered(){

    Form_etatEds *activiteEds = new Form_etatEds();
    activiteEds->show ();

}
