#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QSettings>

#include <QStringBuilder>
#include <QStringList>

class Configuration
{
public:
    Configuration(QString pathFichierIni);

    QString getValue(QString groupe, QString cle);
    void setValue(QString groupe, QString cle, QString valeur);
    QStringList listeCle(QString groupe);
    QStringList listeGroupe();
    bool siGroupeExiste(QString groupe);

private:

    QString fichierIni;
};

#endif // CONFIGURATION_H
