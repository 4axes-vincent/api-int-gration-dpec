#ifndef RAPPORT_H
#define RAPPORT_H

#include <QStringBuilder>
#include <QString>

#include <QTextStream>
#include <QFile>

#include <QDate>

#include <QObject>
#include <QCoreApplication>

#include <QThread>


#include "database.h"

class Rapport : public QThread
{
    Q_OBJECT

public:
    Rapport();

    void genererRapport();
    void envoyerEmail();

private slots:

    void receptionMessage(QString message);

signals:

    void on_traitementFini(QString pathFichierRapport);
    void on_erreur(QString message);
    void on_message(QString message);
};

#endif // RAPPORT_H
