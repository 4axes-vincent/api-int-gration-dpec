#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include <QStringBuilder>

#include <windows.h>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkProxy>

#include <QtXml/QDomDocument>
#include <QtXml/QXmlStreamReader>

#include <QTime>
#include <QTimer>
#include <QDateTime>

#include <QProcess>

#include <QFileInfo>
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QDirIterator>

#include <QStringList>
#include <QList>
#include <QListWidgetItem>
#include <QVector>

#include <QCheckBox>

#include <QDesktopServices>

#include <QPair>

#include <QRegExp>

#include <QCloseEvent>

#include <QtConcurrentRun>
#include <QFuture>
#include <QThread>

#include "decoupage.h"
#include "configuration.h"
#include "database.h"
#include "form_statistiques.h"
#include "rapport.h"
#include "form_etateds.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void debugger(QString titre, QString info);
    void lancerIntegration( QString nomFichierCSV );
    void ecrireLog(QString fichier, QString info);

    void ecrireIHM(QString message);
    void testReponse();
    void deplacerFichier( QString pathFichier, QString repDestination);

    void archiverLog( QFileInfo  infoFichier);
    void importerTrace();
    void envoyerMail();


private slots:

    void on_btn_ouvrirLog_clicked();

    void parserReponse( QString reponse, QString ligneDonnee );

    void destinataireVide(QString ligneDonnee);

    void on_actionStatistiques_triggered();

    void on_actionSauvegarde_logs_triggered();

    void on_actionActivit_EDS_triggered();

public slots:

    void envoyerFichier( QPair<QString, QString> pairDonnee, QString idDestinataire );

    void integrationFini();

    void afficherResultat(int nbLigne);

    void backuperlog();

    void afficherErreur(QString message);

    void afficherMessage(QString message);

    void finRapport(QString fichierRapport);

    void closeEvent(QCloseEvent* event);

signals:

    void lancerDecoupage(QString nomFichierCSV);

    void on_nombreErreurChange( int nombreErreur );

    void on_pause(bool flag);

private:
    Ui::MainWindow *ui;

    QString nomFichier;
    QString finess;
    QString NOM_FICHIER_LOG;
    QString fichierLogCible;
    QString editeur;

    QDateTime dateTime;

    QTime *chronoEnreg;
    QTime *chronoTotal;

    bool fermerIHM;

    int nbErreur;
    int nbRejete;
    int nbEnregistrement;

    QFileInfo fichierCSVInfo;

};

#endif // MAINWINDOW_H
