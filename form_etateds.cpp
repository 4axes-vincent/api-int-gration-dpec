#include "form_etateds.h"
#include "ui_form_etateds.h"

#define PATH_FICHIER_CONFIGURATION      QString(QCoreApplication::applicationDirPath () % "/config.ini")

Form_etatEds::Form_etatEds(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_etatEds)
{
    ui->setupUi(this);
    this->setWindowTitle ( ui->label->text () );
    this->setFixedSize (496,550);

    header << "FINESS" << "Activit�";
    ui->tableActivite->setColumnCount ( header.length () );
    ui->tableActivite->setHorizontalHeaderLabels ( header );
    ui->tableActivite->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    listerFiness();

}

Form_etatEds::~Form_etatEds()
{
    delete ui;
}





//----------------------------------------------------------------------------------------------------------------------
void Form_etatEds::listerFiness(){

    Configuration *config = new Configuration( PATH_FICHIER_CONFIGURATION );

    QStringList listeFiness = config->listeGroupe ();

    listeFiness.removeAt ( listeFiness.indexOf ("Path") );
    listeFiness.removeAt ( listeFiness.indexOf ("Config_mail") );

    for(int i = 0 ; i < listeFiness.length () ; i++){

        ui->tableActivite->insertRow (i);

        QTableWidgetItem *itemFiness = new QTableWidgetItem( listeFiness.at (i) );

        ui->tableActivite->setItem (i, header.indexOf ( "FINESS" ), itemFiness);
        itemFiness->setTextAlignment ( Qt::AlignCenter );

         QTableWidgetItem *itemEtat = new QTableWidgetItem();

        if( config->getValue ( listeFiness.at (i), "etat") == "1" ){

            itemEtat->setText ("Actif");
            itemEtat->setBackgroundColor ( Qt::green );
        }
        else{

            itemEtat->setText ("Inactif");
            itemEtat->setBackgroundColor ( Qt::red );
        }

        itemEtat->setTextAlignment ( Qt::AlignCenter );

        ui->tableActivite->setItem (i, header.indexOf ( "Activit�" ), itemEtat );
    }
}










//----------------------------------------------------------------------------------------------------------------------
void Form_etatEds::on_radioButton_actif_clicked(){


    for(int i = 0 ; i < ui->tableActivite->rowCount () ; i++){

        if( ui->tableActivite->item (i,header.indexOf ( "Activit�" ) )->text () == "Actif" )
            ui->tableActivite->setRowHidden (i, false);
        else
            ui->tableActivite->setRowHidden (i, true);
    }
}










//----------------------------------------------------------------------------------------------------------------------
void Form_etatEds::on_radioButton_inactif_clicked(){

    for(int i = 0 ; i < ui->tableActivite->rowCount () ; i++){

        if( ui->tableActivite->item (i,header.indexOf ( "Activit�" ) )->text () == "Inactif" )
            ui->tableActivite->setRowHidden (i, false);
        else
            ui->tableActivite->setRowHidden (i, true);
    }
}










//----------------------------------------------------------------------------------------------------------------------
void Form_etatEds::on_btn_raz_clicked(){

    ui->tableActivite->setRowCount (0);
    listerFiness ();
}











//----------------------------------------------------------------------------------------------------------------------
void Form_etatEds::on_lineEdit_rchFiness_textChanged(const QString &arg1){

    for(int i = 0 ; i < ui->tableActivite->rowCount () ; i++){

        if( ui->tableActivite->item (i,header.indexOf ( "FINESS" ) )->text ().contains ( arg1 ) )
            ui->tableActivite->setRowHidden (i, false);
        else
            ui->tableActivite->setRowHidden (i, true);
    }
}


//----------------------------------------------------------------------------------------------------------------------
void Form_etatEds::on_tableActivite_doubleClicked(const QModelIndex &index){


}

void Form_etatEds::on_tableActivite_cellDoubleClicked(int row, int column){

    QString nouvelEtat;

    if( ui->tableActivite->item (row, column)->text () == "Actif")
        nouvelEtat = "d�sactiver";
    else
        nouvelEtat = "activer";

    int resultat = QMessageBox::question (NULL,"Modification activit� EDS","Souhaitez-vous " % nouvelEtat % " cet EDS ?",
                                          QMessageBox::Yes | QMessageBox::No);

    if( resultat = QMessageBox::Yes ){

        Configuration *config = new Configuration( PATH_FICHIER_CONFIGURATION );

        if( nouvelEtat == "d�sactiver" )
            config->setValue ( ui->tableActivite->item (row, 0)->text (), "etat", "0" );
        else if( nouvelEtat == "activer" )
            config->setValue ( ui->tableActivite->item (row, 0)->text (), "etat", "1" );

        ui->tableActivite->setRowCount (0);
        listerFiness ();

        delete config;
    }
}
