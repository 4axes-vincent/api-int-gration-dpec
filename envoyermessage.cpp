#include "envoyermessage.h"


#define NOM_FICHIER_ENVOI               QString("fichierRPEC.csv")
#define NOM_FICHIER_LOG                 QString("integration.log")
#define NOM_FICHIER_LOG_ERREUR          QString("erreurIntegration.log")

#define HTTP_DOMAINE_4AXES              QString("domaine")
#define HTTP_DOMAINE_4AXES_VALEUR       QString("4axes")


//Serveur local
//#define ADR_HTTP_INTEGRATION            QString("http://192.168.1.12/transmissions/httpsdocs/api/reconciliercsv")

//Serveur pr�-prod
#define ADR_HTTP_INTEGRATION            QString("https://pp.4axes.net/api/reconciliercsv")

//Serveur prod
//#define ADR_HTTP_INTEGRATION            QString("https://4axes.net/api/reconciliercsv")




envoyerMessage::envoyerMessage(){

}











//----------------------------------------------------------------------------------------------------------------------
void envoyerMessage::envoyerFichier(QPair<QString, QString> pairDonnee, QString idChronoDPEC){


//    ui->textEdit->append (dateTime.currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss") % " - Traitement du : " % IDCHRONO);

    //Cr�ation fichier temporaire pour envoi sur la plateforme
    QFile fichierCSV( NOM_FICHIER_ENVOI );
    if( ! fichierCSV.open ( QIODevice::Text | QIODevice::WriteOnly ) )
        debugger ("Ouverture fichier", "Impossible d'ouvrir le fichier " % NOM_FICHIER_ENVOI % " | Erreur " % fichierCSV.errorString () );
    else{

        QTextStream fluxCSV(&fichierCSV);

        fluxCSV << pairDonnee.first << endl;
        fluxCSV << pairDonnee.second << endl;

        fichierCSV.close ();
    }

    QFile fichier;
    if( !fichier.exists ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_ENVOI ) ){

        ecrireLog (NOM_FICHIER_LOG,"Le fichier a envoy� n'existe pas ou a �t� d�plac� - Int�gration annul�e");
        ecrireLog (NOM_FICHIER_LOG,"-----------------------------------");
        debugger ("Envoi fichier","Le fichier a envoy� n'existe pas ou a �t� d�plac� - Int�gration annul�e");
//        ui->textEdit->setPlainText ("Le fichier " % nomFichierEnvoi % " n'existe pas ou a �t� d�plac� - Int�gration annul�e");
    }
    else{

        ecrireLog (NOM_FICHIER_LOG,"Envoi du fichier sur " % ADR_HTTP_INTEGRATION);
        ecrireLog (NOM_FICHIER_LOG,"Int�gration RPEC (IDCHRONO : " % idChronoDPEC % ")" );

        QProcess processus;

        //Lancer CURL avec idMesage / pathFichier / url 4axes.net
        QString cmd = QCoreApplication::applicationDirPath ()
                % "/CURL/curl.exe -k -H " % HTTP_DOMAINE_4AXES % ":" % HTTP_DOMAINE_4AXES_VALEUR
                % " --form idChrono="% idChronoDPEC % " --form \"fichier=@" % QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_ENVOI
                % "\" " % ADR_HTTP_INTEGRATION;

        debugger ("",cmd);


        //Lancement de la commande avec cUrl.exe
        processus.start (cmd);

        //On lit la sortie pour connaitre le resultat de notre requ�te
        processus.setProcessChannelMode(QProcess::MergedChannels);
        processus.waitForFinished();
        QString resultatPrompt = processus.readAll ();

        if( resultatPrompt.isEmpty () )
            ecrireLog ( NOM_FICHIER_LOG, "Erreur processus : " % processus.errorString () );

        emit on_fichierEnvoyer ( resultatPrompt, idChronoDPEC );
    }
}




















//----------------------------------------------------------------------------------------------------------------------
void envoyerMessage::debugger(QString titre, QString info){

    QMessageBox::information (NULL, titre, info);
}
















//----------------------------------------------------------------------------------------------------------------------
void envoyerMessage::ecrireLog(QString nomFichier, QString info){

    QFile fichierLog(QCoreApplication::applicationDirPath () % "/" + nomFichier);
    if( ! fichierLog.open ( QIODevice::Text | QIODevice::WriteOnly | QIODevice::Append ) )
        debugger ("Ouverture fichier log","Impossible d'ouvrir le fichier " % nomFichier % " - " % fichierLog.errorString ());

    else{

        QDateTime dateTime;

        QTextStream fluxLog(&fichierLog);

        fluxLog << dateTime.currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss")
                    << " >> "
                    << info
                    << endl;

        fichierLog.close ();
    }
}
