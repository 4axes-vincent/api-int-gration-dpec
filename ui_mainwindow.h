/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Tue 26. Jul 16:20:10 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionStatistiques;
    QAction *actionSauvegarde_logs;
    QAction *actionActivit_EDS;
    QWidget *centralWidget;
    QTextEdit *textEdit;
    QLabel *label_titre;
    QLabel *label_adrSite;
    QLineEdit *lineEdit_adrSite;
    QLabel *label_nomFichier;
    QLineEdit *lineEdit_nomFichier;
    QFrame *line;
    QLabel *label_tempsRequete;
    QTextEdit *zoneErreur;
    QPushButton *btn_ouvrirLog;
    QLabel *label_nbEnregistrement;
    QLineEdit *lineEdit_nbEnregistrement;
    QLabel *label_nbErreur;
    QLineEdit *lineEdit_nbErreur;
    QLabel *label_nbRPEC;
    QLineEdit *lineEdit_nbRPEC;
    QLabel *label_idCH;
    QLineEdit *lineEdit_idCH;
    QMenuBar *menuBar;
    QMenu *menuStatistiques;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(684, 574);
        actionStatistiques = new QAction(MainWindow);
        actionStatistiques->setObjectName(QString::fromUtf8("actionStatistiques"));
        actionSauvegarde_logs = new QAction(MainWindow);
        actionSauvegarde_logs->setObjectName(QString::fromUtf8("actionSauvegarde_logs"));
        actionActivit_EDS = new QAction(MainWindow);
        actionActivit_EDS->setObjectName(QString::fromUtf8("actionActivit_EDS"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(20, 120, 641, 311));
        label_titre = new QLabel(centralWidget);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(300, 10, 121, 16));
        label_adrSite = new QLabel(centralWidget);
        label_adrSite->setObjectName(QString::fromUtf8("label_adrSite"));
        label_adrSite->setGeometry(QRect(20, 30, 51, 21));
        lineEdit_adrSite = new QLineEdit(centralWidget);
        lineEdit_adrSite->setObjectName(QString::fromUtf8("lineEdit_adrSite"));
        lineEdit_adrSite->setGeometry(QRect(80, 30, 211, 20));
        label_nomFichier = new QLabel(centralWidget);
        label_nomFichier->setObjectName(QString::fromUtf8("label_nomFichier"));
        label_nomFichier->setGeometry(QRect(20, 60, 81, 20));
        lineEdit_nomFichier = new QLineEdit(centralWidget);
        lineEdit_nomFichier->setObjectName(QString::fromUtf8("lineEdit_nomFichier"));
        lineEdit_nomFichier->setGeometry(QRect(100, 60, 191, 20));
        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(350, 30, 20, 81));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        label_tempsRequete = new QLabel(centralWidget);
        label_tempsRequete->setObjectName(QString::fromUtf8("label_tempsRequete"));
        label_tempsRequete->setGeometry(QRect(30, 540, 171, 20));
        zoneErreur = new QTextEdit(centralWidget);
        zoneErreur->setObjectName(QString::fromUtf8("zoneErreur"));
        zoneErreur->setGeometry(QRect(460, 30, 201, 81));
        btn_ouvrirLog = new QPushButton(centralWidget);
        btn_ouvrirLog->setObjectName(QString::fromUtf8("btn_ouvrirLog"));
        btn_ouvrirLog->setGeometry(QRect(590, 440, 75, 23));
        label_nbEnregistrement = new QLabel(centralWidget);
        label_nbEnregistrement->setObjectName(QString::fromUtf8("label_nbEnregistrement"));
        label_nbEnregistrement->setGeometry(QRect(20, 440, 161, 16));
        lineEdit_nbEnregistrement = new QLineEdit(centralWidget);
        lineEdit_nbEnregistrement->setObjectName(QString::fromUtf8("lineEdit_nbEnregistrement"));
        lineEdit_nbEnregistrement->setGeometry(QRect(180, 440, 61, 20));
        label_nbErreur = new QLabel(centralWidget);
        label_nbErreur->setObjectName(QString::fromUtf8("label_nbErreur"));
        label_nbErreur->setGeometry(QRect(20, 470, 101, 16));
        lineEdit_nbErreur = new QLineEdit(centralWidget);
        lineEdit_nbErreur->setObjectName(QString::fromUtf8("lineEdit_nbErreur"));
        lineEdit_nbErreur->setGeometry(QRect(180, 470, 61, 20));
        label_nbRPEC = new QLabel(centralWidget);
        label_nbRPEC->setObjectName(QString::fromUtf8("label_nbRPEC"));
        label_nbRPEC->setGeometry(QRect(20, 500, 121, 16));
        lineEdit_nbRPEC = new QLineEdit(centralWidget);
        lineEdit_nbRPEC->setObjectName(QString::fromUtf8("lineEdit_nbRPEC"));
        lineEdit_nbRPEC->setGeometry(QRect(180, 500, 61, 20));
        label_idCH = new QLabel(centralWidget);
        label_idCH->setObjectName(QString::fromUtf8("label_idCH"));
        label_idCH->setGeometry(QRect(20, 90, 81, 21));
        lineEdit_idCH = new QLineEdit(centralWidget);
        lineEdit_idCH->setObjectName(QString::fromUtf8("lineEdit_idCH"));
        lineEdit_idCH->setGeometry(QRect(110, 90, 181, 20));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 684, 21));
        menuStatistiques = new QMenu(menuBar);
        menuStatistiques->setObjectName(QString::fromUtf8("menuStatistiques"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuStatistiques->menuAction());
        menuStatistiques->addAction(actionStatistiques);
        menuStatistiques->addAction(actionSauvegarde_logs);
        menuStatistiques->addAction(actionActivit_EDS);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionStatistiques->setText(QApplication::translate("MainWindow", "Statistiques", 0, QApplication::UnicodeUTF8));
        actionSauvegarde_logs->setText(QApplication::translate("MainWindow", "Sauvegarder log", 0, QApplication::UnicodeUTF8));
        actionActivit_EDS->setText(QApplication::translate("MainWindow", "Activer/D\303\251sactiver EDS", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("MainWindow", "API d'int\303\251gration - DPEC", 0, QApplication::UnicodeUTF8));
        label_adrSite->setText(QApplication::translate("MainWindow", "Adresse : ", 0, QApplication::UnicodeUTF8));
        label_nomFichier->setText(QApplication::translate("MainWindow", "Nom du fichier : ", 0, QApplication::UnicodeUTF8));
        label_tempsRequete->setText(QString());
        btn_ouvrirLog->setText(QApplication::translate("MainWindow", "Ouvrir log", 0, QApplication::UnicodeUTF8));
        label_nbEnregistrement->setText(QApplication::translate("MainWindow", "Nombre d'enregistrement total : ", 0, QApplication::UnicodeUTF8));
        label_nbErreur->setText(QApplication::translate("MainWindow", "Nombre d'erreur :", 0, QApplication::UnicodeUTF8));
        label_nbRPEC->setText(QApplication::translate("MainWindow", "Nombre RPEC intr\303\251gr\303\251 :", 0, QApplication::UnicodeUTF8));
        label_idCH->setText(QApplication::translate("MainWindow", "Identifiant C.H :", 0, QApplication::UnicodeUTF8));
        menuStatistiques->setTitle(QApplication::translate("MainWindow", "Edition", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
