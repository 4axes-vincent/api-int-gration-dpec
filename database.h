#ifndef DATABASE_H
#define DATABASE_H

#define UTILISATEUR_BDD             QString("4axes")
#define MOT_DE_PASSE_BDD            QString("fgbmo78")
#define DATABASE                    QString("QSQLITE")
#define HOSTNAME                    QString("localhost")
#define DATABASE_NAME               QString(QCoreApplication::applicationDirPath () % "/base_integration")

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlResult>

#include <QStringBuilder>
#include <QStringList>
#include <QString>

#include <QDateTime>

#include <QObject>

#include <QCoreApplication>

#include <QPair>

class Database : public QObject
{
    Q_OBJECT

public:
    Database();

    void creerTable();
    bool ajouterDonnee(QString table, QStringList listeValeurs);
    QStringList getStats();
    QString getStats(QString colonne );
    QStringList getStatsComplete(QString table, QString finess, QString date );
    QStringList listerColonne(QString table, QString colonne);


signals:

    void on_message(QString message);

private:

    QSqlDatabase db;
};

#endif // DATABASE_H
