#ifndef DECOUPAGE_H
#define DECOUPAGE_H


#include <QObject>
#include <QThread>
#include <QMutex>
#include <QtConcurrentRun>

#include <QFile>
#include <QTextStream>

#include <QStringBuilder>
#include <QStringList>

#include <QMessageBox>

#include <QCoreApplication>

#include <QDateTime>

#include <QPair>

#include <QDir>
#include <QFileInfo>



class decoupage : public QThread
{
    Q_OBJECT

public:
    decoupage();

    void decouperFichier(QString fichierCSV);

signals:
    void on_ligneDecouper(QPair<QString,QString>, QString);

    void on_traitementFini();

    void on_nombreLigneChange( int nombreLigne );

    void on_erreurDestinataire(QString ligne);

    void on_erreur(QString message);

private slots:

    void setPause(bool flag);

private:

    int nbLigne;
    bool pause;
    QDateTime dateTime;
};

#endif // DECOUPAGE_H
