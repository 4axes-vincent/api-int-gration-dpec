#include "database.h"

Database::Database(){

    creerTable ();
}



//----------------------------------------------------------------------------------------------------------------------
void Database::creerTable(){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                       % db.lastError().text() );
    }
    else{

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_apiIntegration  ( "
                " finess VARCHAR(255),"
                " dateIntegration VARCHAR(255),"
                " heureIntegration VARCHAR(255),"
                " nbDpec INTEGER,"
                " nbErreur INTEGER,"
                " nbRejete INTEGER )");
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}






//----------------------------------------------------------------------------------------------------------------------
bool Database::ajouterDonnee(QString table, QStringList listeValeurs){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text() );
        return( false );
    }
    else{

        if( listeValeurs.length () == 6 ){

            QSqlQuery sqlQuery;
            QString requete = "INSERT INTO " % table % " (finess, dateIntegration, heureIntegration, nbDpec, nbErreur, nbRejete) "
                    " VALUES ('" % listeValeurs.at (0) % "', '" % listeValeurs.at (1) % "', '" % listeValeurs.at (2)
                    % "', " % listeValeurs.at (3) % ", " % listeValeurs.at (4) % ", " % listeValeurs.at (5) % ")";

            if( sqlQuery.exec ( requete ) )
                return( true );
            else{

                emit on_message ("Impossible d'ex�cuter la requete [ " % requete % " ] - Erreur : " % sqlQuery.lastError ().text () );
                return( false );
            }
        }
        else{

            emit on_message ("Erreur lors de l'ajout des donn�es en base de donn�es - V�rifier les valeurs de la liste" );
            return( false );
        }
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}















//----------------------------------------------------------------------------------------------------------------------
QStringList Database::getStats(){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    QStringList listeDonnees;

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text() );
        return( listeDonnees );
    }
    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT finess, SUM(nbDpec), SUM(nbErreur), SUM(nbRejete) FROM tbl_apiIntegration WHERE dateIntegration = '"
                % QDate::currentDate ().toString ("dd/MM/yyyy") % "' GROUP BY finess";

        if( sqlQuery.exec ( requete ) ){

            QString donnees;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                donnees = sqlQuery.value (0).toString ()
                        % ";" % sqlQuery.value (1).toString ()
                        % ";" % sqlQuery.value (2).toString ()
                        % ";" % sqlQuery.value (3).toString ();

                listeDonnees.push_back ( donnees );

                donnees.clear ();
            }

            return( listeDonnees );
        }
        else{

            emit on_message ("Impossible d'ex�cuter la requete [ " % requete % " ] - Erreur : " % sqlQuery.lastError ().text () );
            return( listeDonnees );
        }
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}























//----------------------------------------------------------------------------------------------------------------------
QString Database::getStats(QString colonne){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text() );
        return( "-1" );
    }
    else{

        QString requete;
        QSqlQuery sqlQuery;

        requete = "SELECT SUM( " % colonne % " ) FROM tbl_apiIntegration WHERE dateIntegration = '" % QDate::currentDate ().toString ("dd/MM/yyyy") % "'";

        if( sqlQuery.exec ( requete ) ){

            QString stats;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                stats = sqlQuery.value (0).toString ();
            }
            return( stats );
        }
        else{

            emit on_message ("Impossible d'ex�cuter la requete [ " % requete % " ] - Erreur : " % sqlQuery.lastError ().text () );
            return( "-1" );
        }
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}























//----------------------------------------------------------------------------------------------------------------------
QStringList Database::getStatsComplete(QString table, QString finess, QString date){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    QStringList listeDonnees;

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text() );
        return( listeDonnees );
    }
    else{

        QString requete;
        QSqlQuery sqlQuery;

        if( finess != "-1" )
            requete = "SELECT finess, dateIntegration, heureIntegration, nbDpec, nbErreur, nbRejete FROM "
                    % table % " WHERE finess = '" % finess % "' AND dateIntegration = '" % date % "'";
        else
            requete = "SELECT finess, dateIntegration, heureIntegration, nbDpec, nbErreur, nbRejete FROM "
                    % table % " WHERE dateIntegration = '" % date % "'";


        if( sqlQuery.exec ( requete ) ){

            QString donnees;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                donnees = sqlQuery.value (0).toString ()
                        % ";" % sqlQuery.value (1).toString ()
                        % ";" % sqlQuery.value (2).toString ()
                        % ";" % sqlQuery.value (3).toString ()
                        % ";" % sqlQuery.value (4).toString ()
                        % ";" % sqlQuery.value (5).toString () ;

                listeDonnees.push_back (donnees);

                donnees.clear ();
            }

            return( listeDonnees );
        }
        else{

            emit on_message ("Impossible d'ex�cuter la requete [ " % requete % " ] - Erreur : " % sqlQuery.lastError ().text () );
            return( listeDonnees );
        }
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}















//----------------------------------------------------------------------------------------------------------------------
QStringList Database::listerColonne(QString table, QString colonne){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    QStringList listeValeur;

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text() );
        return( listeValeur );
    }
    else{

        QString requete = "SELECT " % colonne % " FROM " % table % " ORDER BY finess ASC";
        QSqlQuery sqlQuery;

        if( sqlQuery.exec ( requete ) ){

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                listeValeur.push_back ( sqlQuery.value (0).toString () );
            }

            return( listeValeur );
        }
        else{

            emit on_message ("Impossible d'ex�cuter la requete [ " % requete % " ] - Erreur : " % sqlQuery.lastError ().text () );
            return( listeValeur );
        }
    }

    //Cloture de la connexion
    db.commit();
    db.removeDatabase ( DATABASE_NAME  );
    db.close();
}
