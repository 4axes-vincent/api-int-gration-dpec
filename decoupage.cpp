#include "decoupage.h"

//----------------------------------------------------------------------------------------------------------------------





#define NOM_FICHIER_ENVOI               QString("fichierRPEC.csv")
#define NOM_FICHIER_LOG                 QString("integration.log")
#define NOM_FICHIER_LOG_ERREUR          QString("erreurIntegration.log")





//----------------------------------------------------------------------------------------------------------------------
decoupage::decoupage(){
}
















//----------------------------------------------------------------------------------------------------------------------
void decoupage::decouperFichier(QString fichierCSV){

    QFile fichier( fichierCSV );
    if( !fichier.open ( QIODevice::Text | QIODevice::ReadOnly ) )
        emit on_erreur ("Impossible d'ouvrir le fichier " % fichierCSV % " | Erreur " % fichier.errorString () );
    else{

        QTextStream flux(&fichier);

        QString ligneCourante;
        QString ligneEntete;
        QString destinataire;

        QStringList ligneDecoupee;

        nbLigne = 0;

        pause = false;

        //Tant qu'on est pas arriv� � la fin du fichier d'entr�e
        while( !flux.atEnd () ){

            while( pause ){

                QThread::currentThread ()->msleep (1000);
            }

            //Lecture de la ligne
            ligneCourante = flux.readLine ();

            //V�rification ligne vide
            if( !ligneCourante.isEmpty () ){

                //Si ent�te
                if( ligneCourante.contains ( "DATEJOUR;" ) )
                    ligneEntete = ligneCourante;

                else{

                    nbLigne++;

                    QPair<QString, QString> pair;

                    pair.first = ligneEntete;
                    pair.second = ligneCourante;

                    ligneDecoupee = ligneCourante.split (";");
                    destinataire = ligneDecoupee.at (4);

                    if( !destinataire.isEmpty () ){

                        QMutex lock;
                        lock.lock ();

                        emit on_ligneDecouper( pair, destinataire );

                        QThread::currentThread ()->msleep (100);

                        lock.unlock ();
                    }
                    else
                        emit on_erreurDestinataire (ligneCourante);

                    emit on_nombreLigneChange(nbLigne);
                }
            }
            QThread::currentThread ()->msleep (1000);
        }
        emit on_traitementFini ();
    }
}










//----------------------------------------------------------------------------------------------------------------------
void decoupage::setPause(bool flag){

    pause = flag;
}

