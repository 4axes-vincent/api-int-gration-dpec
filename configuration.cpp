#include "configuration.h"

Configuration::Configuration(QString pathFichierIni){

    fichierIni = pathFichierIni;
}



//---------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la valeur d'une cl� [cle] appartement � un groupe [groupe] contenu dans un fichier [fichierIni]
  */
QString Configuration::getValue(QString groupe, QString cle){

    QSettings settings( fichierIni, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    return( settings.value ( cle, "-1").toString () );
}








//---------------------------------------------------------------------------------------------------------------------
void Configuration::setValue(QString groupe, QString cle, QString valeur){

    QSettings settings( fichierIni, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    settings.setValue ( cle, valeur );
}







//---------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la liste des cl�s du groupe [groupe] contenu dans le fichier [fichierIni]
  */
QStringList Configuration::listeCle(QString groupe){

    QSettings settings( fichierIni, QSettings::IniFormat);
    settings.beginGroup ( groupe );
    return( settings.allKeys () );
}







//---------------------------------------------------------------------------------------------------------------------
QStringList Configuration::listeGroupe(){

     QSettings settings( fichierIni, QSettings::IniFormat);
     return( settings.childGroups () );
}





//---------------------------------------------------------------------------------------------------------------------
/*
  Retourne TRUE si le groupe [groupe] existe dans le fichier
  */
bool Configuration::siGroupeExiste(QString groupe){

    QSettings settings( fichierIni, QSettings::IniFormat);
    return( settings.childGroups ().contains (groupe) );
}
