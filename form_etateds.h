#ifndef FORM_ETATEDS_H
#define FORM_ETATEDS_H

#include <QWidget>
#include <QTableWidgetItem>
#include <QStringBuilder>
#include <QStringList>
#include <QMessageBox>


#include "configuration.h"


namespace Ui {
class Form_etatEds;
}

class Form_etatEds : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_etatEds(QWidget *parent = 0);
    ~Form_etatEds();

    void listerFiness();
    
private slots:
    void on_radioButton_actif_clicked();

    void on_radioButton_inactif_clicked();

    void on_btn_raz_clicked();

    void on_lineEdit_rchFiness_textChanged(const QString &arg1);

    void on_tableActivite_doubleClicked(const QModelIndex &index);

    void on_tableActivite_cellDoubleClicked(int row, int column);

private:
    Ui::Form_etatEds *ui;

    QStringList header;
};

#endif // FORM_ETATEDS_H
